<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpresslearningdb');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

// Increasing memory limit
define( 'WP_MEMORY_LIMIT', '256M' );
define( 'WP_MAX_MEMORY_LIMIT' , '256M' );


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'M7,-s67.0$MPd{HT~sByh|{w@+}n[6vG>-6z6*UxSg2b.s)$2x,M%@hM36g1^lc=');
define('SECURE_AUTH_KEY',  '3Yl)2vw/io_`6q%#X0#6F;TAR9kK@>xP=A]7=tQV@0<B3Sdt,goH-;3*~!I|YBd|');
define('LOGGED_IN_KEY',    'C]6j1BRdSMY<Tqk^>>{M*1{aP,qe1vOBMb2MX))X`Q7_;kiri5h@N5F-Q[QTmeY-');
define('NONCE_KEY',        'WhVj.3(]FYxnXd5K}FY;#I.Wp#GtW:Z~w|vDz{JVi5t@[?R(w^;i;kuqjvRvjLWp');
define('AUTH_SALT',        'd6:Wq2qW f1 E9:&ulX^%/hlbU0Qp{rXqB/ZEF={j(XZwqa4oQOM(=_nSC#|XCQF');
define('SECURE_AUTH_SALT', 'UfK!uTsRWmpf*A`nqO@2xTX>Ga(:.fGkznyVf5C oo?5,D!(j@5!OPS(hO16{r@7');
define('LOGGED_IN_SALT',   'lPUo[nG/:t`<+mrx/f#m,n3KIC.V?5O-dheeR,YlTWsIlyJLi Uc@!b?bXH3?._O');
define('NONCE_SALT',       'FL::Ipun%C845D;w%52{4nEp48$t?ZUI/QkE:xyZ!6w5ZeJ@n)Uq3-&7{Uho#JP%');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpl_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

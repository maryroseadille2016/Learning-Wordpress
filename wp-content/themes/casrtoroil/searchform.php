
<form role="search" method="get" id="searchform" action="<?php home_url(); ?>">
	<div class="input-group searchform">
	    <input type="text" class="form-control" name="s" id="s" placeholder="<?php the_search_query();?>">
	    <span class="input-group-btn">
	      <button class="btn btn-default" type="submit" id="searchsubmit">
	        <span class="glyphicon glyphicon-search"></span>
	      </button>
	    </span>
    </div>
</form>

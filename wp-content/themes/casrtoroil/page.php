<?php 

get_header(); //HEADER

if (have_posts()) :
	while (have_posts()) : the_post(); ?>

      
		
		<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 main-wrapper">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-content">
				<article class="post page" data-id="<?php echo get_the_ID() ?>">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</article>
			</div>
			<?php 

			if ( is_page() && $post->post_parent ) {
				ECHO "PAGE AND CHILD";
				?>
				<div class="granchild-content">
					<div class="col-xs-12 col-md-12">
						<div class="row grandchild-indiv">
						<div class="col-sm-6 col-md-6 col-xs-12 image-container">
							<img src="http://placehold.it/500x300" style="height:200px; margin-left:-15px;" />
						</div>
						<div class="col-sm-6 col-md-6 col-xs-12">  
							<h3>Hello World</h3>
							 <p style="font-size:10px; color:#03225C;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed hendrerit adipiscing blandit. Aliquam placerat, velit a fermentum fermentum, mi felis vehicula justo, a dapibus quam augue non massa.   </p>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-md-12">
						<div class="row grandchild-indiv">
						<div class="col-sm-6 col-md-6 col-xs-12 image-container">
							<img src="http://placehold.it/500x300" style="height:200px; margin-left:-15px;" />
						</div>
						<div class="col-sm-6 col-md-6 col-xs-12">  
							<h3>Hello World</h3>
							 <p style="font-size:10px; color:#03225C;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed hendrerit adipiscing blandit. Aliquam placerat, velit a fermentum fermentum, mi felis vehicula justo, a dapibus quam augue non massa.   </p>
							</div>
						</div>
					</div>
				</div>


				<?php

			}else {
				echo "parent";   ?>
				<div class="child-content">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			          <div class="child-indiv">
			            <img src="http://placehold.it/500x300" alt="">
			              <div class="caption">
			                <h4>Thumbnail label</h4>
			                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, soluta, eligendi doloribus sunt minus amet sit debitis repellat. Consectetur, culpa itaque odio similique suscipit</p>
			                <p><a href="#" class="btn btn-info btn-xs" role="button">Button</a> <a href="#" class="btn btn-default btn-xs" role="button">Button</a></p>
			            </div>
			          </div>
			        </div>

			        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			          <div class="child-indiv">
			            <img src="http://placehold.it/500x300" alt="">
			              <div class="caption">
			                <h4>Thumbnail label</h4>
			                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, soluta, eligendi doloribus sunt minus amet sit debitis repellat. Consectetur, culpa itaque odio similique suscipit</p>
			                <p><a href="#" class="btn btn-info btn-xs" role="button">Button</a> <a href="#" class="btn btn-default btn-xs" role="button">Button</a></p>
			            </div>
			          </div>
			        </div>
				</div>



				<?php
			}

			?>
			


		</div>









	<?php endwhile;
else :
	echo '<p>No content found</p>';
endif;


?>



<?php



//SIDEBAR
get_sidebar(); 

?>




<?php


get_footer();
?>


<?php

// function get_style(){
// 	wp_register_script( 'bootstrap_css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
// 	wp_register_script( 'fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
// 	wp_register_script( 'main_css', get_stylesheet_uri() . 'style.css');
// 	wp_enqueue_script( 'bootstrap_css' );
// 	wp_enqueue_script( 'fontawesome' );
// 	wp_enqueue_script( 'main_css' );
// 		wp_register_script( 'bootstrap_js','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
// 	wp_enqueue_script( 'bootstrap_js' );
// }
// add_action( 'wp_enqueue_scripts', 'get_style' );

	function LearningWordpress_css(){
		
		//cdn css
		wp_enqueue_style('bootstrap_css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
		wp_enqueue_style('fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
		//main css
		wp_enqueue_style( 'style', get_stylesheet_uri());


		//cdn scripts
		wp_enqueue_script('jquery_js','https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js');
		wp_enqueue_script('bootstrap_js','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');


		//main js
		wp_enqueue_script('js',get_stylesheet_directory_uri() .'/assets/js/main.js');
	}
	add_action( 'wp_enqueue_scripts', 'LearningWordpress_css' );



//Get top ancestor
function get_top_ancestor_id() {
	global $post;
	if($post->post_parent) {
		$ancestors = array_reverse(get_post_ancestors( $post->ID ));
		return $ancestors[0];
	}
	return $post->ID;
}


//Does page have children?
function has_children() {
	global $post;
	$pages = get_pages('child_of='.$post->ID);
	return count($pages);
}



function wordpress_setup() {
	//Navigation Menus
	register_nav_menus( array( 
	   'header' => __('Primary Menu'),
	   'footer' => __('Footer Menu')
	 ) );

	//Add featured image support
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'small-thumbnail', 180, 120, true );

	// add_image_size( 'banner-image', 920, 210, true );
	add_image_size( 'banner-image', 920, 210, array('left','top') );

	//Add post format support
	add_theme_support( 'post-formats', array('aside', 'gallery', 'link') );

	// Adding Header support
	add_theme_support( 'custom-header');

	// Adding Logo support
	add_theme_support( 'custom-logo' );
}
add_action( 'after_setup_theme', 'wordpress_setup' );



// Add widgets Location
function ourWidgetsInit() {
	register_sidebar( array( 
		'name' => 'Sidebar',
		'id' => 'mainsidebar'
	 ) );
}
add_action( 'widgets_init', 'ourWidgetsInit' );


// Register custom navigation walker
require_once('wp_bootstrap_navwalker.php');
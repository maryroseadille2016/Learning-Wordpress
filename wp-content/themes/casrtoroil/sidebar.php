<?php
get_header(); //HEADER ?>

<div class="row">
<div class="col-lg-4 col-md-12 col-sm-1z col-xs-12 side-warapper">
  <div class="wrapper-sidebar scrollspy">
    <div class="col-sm-12 sidenav search-nav">
      <?php get_search_form();  ?>
    </div>

    <div class="col-sm-12 sidenav amazon-nav" >
      <h4>Amazon</h4>
    </div>
    <div class="col-sm-12 sidenav">
      <h4>What's New</h4>
      <?php
      global $post;
      $siblings = new WP_Query( array(
          'post_type'      => 'page',
          'orderby'        => 'date',
          'posts_per_page' => 5
      ) );

      if ( $siblings->have_posts() ) : ?>
          <!-- A container element can go here. -->
              <?php while( $siblings->have_posts() ) : $siblings->the_post(); ?>
                  <!-- Use template tags here to display post data. -->
                  <a href="<?php the_permalink(); ?>"><p><?php the_title()?></p></a>
              <?php endwhile; ?>
          <!-- Close container element. -->
      <?php endif; wp_reset_query(); ?>
    </div>

    <div class="col-sm-12 sidenav sticky-nav sticky " id="sticky" >
      <h4>Latest Reviews</h4>
      
      
    </div>
  </div>

</div>
</div>


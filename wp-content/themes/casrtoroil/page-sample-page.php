<?php 

get_header();

if (have_posts()) :
	while (have_posts()) : the_post(); ?>
	<article data-id="<?php echo get_the_ID() ?>" class="post page">
		<h1><?php the_title(); ?> - diff template</h1>
		<?php the_content(); ?>
	</article>

	<?php endwhile;
else :
	echo '<p>No content found</p>';

endif;

get_footer();
?>

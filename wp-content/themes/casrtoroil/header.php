<!DOCTYPE html>
<html <?php language_attributes(); ?> >
<head>
	<meta charset="<?php bloginfo( 'charset' );?>">
	<meta name="viewport" content="width=device-width">
	<title><?php bloginfo( 'name' );?></title>
	<?php wp_head(); ?> 
</head>
<body <?php body_class(); ?> data-spy="scroll" data-target=".scrollspy">
	 <nav id="mainNav" class="navbar transparent navbar-opacity navbar-fixed-top">
        <div class="container navbar-inner">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span> <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="<?php  echo home_url(); ?>">
                <?php
                	$custom_logo_id = get_theme_mod( 'custom_logo' );
					$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
					
                ?>
                <image src="<?php echo $image[0]; ?>" />
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse menulist" id="bs-example-navbar-collapse-1">
				<?php /* Primary navigation */
					wp_nav_menu( array(
					  'menu' => 'top_menu',
					  'theme_location' => 'header',
					  'depth' => 2,
					  'container' => false,
					  'menu_class' => 'nav navbar-nav navbar-right',
					  //Process nav menu using our custom nav walker
					  'walker' => new wp_bootstrap_navwalker())
					);
				?>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    <header style="background-image: url('<?php echo header_image(); ?>'); height: 500px;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="header-content">
                        <div class="header-content-inner">
                            <h1>New Age is an app landing page that will help you beautifully showcase your new mobile app, or anything else!</h1>

                            <a href="#download" class="btn btn-outline btn-xl page-scroll">Start Now for Free!</a>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </header>



	<div class="container">
 		<div class="row content">

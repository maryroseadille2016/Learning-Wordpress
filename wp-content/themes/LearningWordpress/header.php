<!DOCTYPE html>
<html <?php language_attributes(); ?> >
<head>
	<meta charset="<?php bloginfo( 'charset' );?>">
	<meta name="viewport" content="width=device-width">
	<title><?php bloginfo( 'name' );?></title>
	<?php wp_head(); ?> 
</head>
<body <?php body_class(); ?> >

	<header class="site-header">

		<div class="hd-search">
			<?php get_search_form();  ?>
		</div>

		<h1 data-id="<?php echo get_the_ID() ?>"><a href="<?php echo home_url();?>"><?php bloginfo( 'name' ); ?></a></h1>
		<p><?php bloginfo( 'description' ); ?></p>

		<?php if(is_page()) {



		} ?>

		<nav class="site-nav">
			<?php
				$args = array('theme_location' => 'header');
			?>
			<?php wp_nav_menu( $args ); ?>
		</nav>
	</header>

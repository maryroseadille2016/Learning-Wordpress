<?php
/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress
 */

/**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */
define('WP_USE_THEMES', true);

/** Loads the WordPress Environment and Template */
require( dirname( __FILE__ ) . '/wp-blog-header.php' );

?>


		<h1 data-id="<?php echo get_the_ID() ?>"><a href="<?php echo home_url();?>"><?php echo get_the_title();  ?></a></h1>
		<p><?php bloginfo( 'description' ); ?></p>




		<?php if(is_page()) {
			echo "page";
		} ?>


<!-- <div class="col-sm-9">





  <h4><small>RECENT POSTS</small></h4>
  <hr>
  <h2>I Love Food</h2>
  <h5><span class="glyphicon glyphicon-time"></span> Post by Jane Dane, Sep 27, 2015.</h5>
  <h5><span class="label label-danger">Food</span> <span class="label label-primary">Ipsum</span></h5><br>
  <p>Food is my passion. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
  <br><br>
</div>
 -->

<?php //get_sidebar('all'); ?>
